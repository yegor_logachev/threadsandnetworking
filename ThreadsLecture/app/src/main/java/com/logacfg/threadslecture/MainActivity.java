package com.logacfg.threadslecture;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String THREAD_LOG = "ThreadLog";
    private static final int THREAD_MSG = 0;

    private volatile Integer count = 0;
    private TextView textView;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Looper.myLooper()
        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                textView.setText(String.valueOf(message.arg1));
                return true;
            }
        });
        textView = (TextView) findViewById(R.id.textView);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    if (Thread.interrupted()) {
                        Log.e(THREAD_LOG, "Thread1 iterapted");
                        break;
                    } else {
                        try {
                            Thread.sleep(500);
                            Message message = handler.obtainMessage(THREAD_MSG, i, 0);
                            handler.sendMessage(message);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                }
                            });
                        } catch (InterruptedException e) {
                            Log.e(THREAD_LOG, "Thread1 iterapted on exception");
                            e.printStackTrace();
                            break;
                        }
                    }
                }
            }
        });
        thread.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(null);
        handler.removeMessages(THREAD_MSG);
        handler.removeCallbacksAndMessages(null);
    }
}
