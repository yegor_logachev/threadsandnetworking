package com.logacfg.mvplecture.add_user;

import com.logacfg.mvplecture.BasePresenter;
import com.logacfg.mvplecture.BaseView;
import com.logacfg.mvplecture.model.User;

/**
 * Created by Yegor on 8/5/17.
 */

public interface AddUserContract {

    interface Presenter extends BasePresenter {

        void addUser(User user);

    }

    interface View extends BaseView<Presenter> {
        void onUserAdded(long id);
    }
}
