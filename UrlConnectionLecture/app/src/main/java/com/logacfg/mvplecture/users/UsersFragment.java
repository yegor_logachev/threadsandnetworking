package com.logacfg.mvplecture.users;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logacfg.mvplecture.R;
import com.logacfg.mvplecture.add_user.AddUserActivity;
import com.logacfg.mvplecture.add_user.AddUserFragment;
import com.logacfg.mvplecture.model.User;

import java.util.List;

/**
 * Created by Yegor on 7/29/17.
 */

public class UsersFragment extends Fragment implements UsersContract.View, View.OnClickListener {

    private static final int ADD_USER_REQUEST_CODE = 0;

    private UsersContract.Presenter presenter;
    private UsersRecyclerAdapter adapter;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_users, container, false);
        recyclerView = rootView.findViewById(R.id.listView);
        adapter = new UsersRecyclerAdapter(getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        rootView.findViewById(R.id.buttonAddUser).setOnClickListener(this);
        return rootView;
    }

    @Override
    public void setPresenter(UsersContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.start();
    }

    @Override
    public void showUsers(final List<User> users) {
        adapter.setUsers(users);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getContext(), AddUserActivity.class);
        startActivityForResult(intent, ADD_USER_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_USER_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            long id = AddUserFragment.getUserId(data);
            User user = presenter.getUserById(id);
            addUser(user);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void addUser(User user) {
        if (user != null) {
//            adapter.addUserAsFirst(user);
            recyclerView.scrollToPosition(0);
        }
    }
}
