package com.logacfg.mvplecture;

/**
 * Created by Yegor on 7/29/17.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
